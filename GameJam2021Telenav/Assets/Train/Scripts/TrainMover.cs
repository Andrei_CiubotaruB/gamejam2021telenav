using UnityEngine;

namespace Train.Scripts
{
    public class TrainMover : MonoBehaviour
    {
        [SerializeField] private float moveSpeed = 5;
        [SerializeField] private float turnSpeed = 5;
        
        private Rigidbody rb;
        private bool grounded;
        
        // Start is called before the first frame update
        void Start()
        {
            rb = GetComponent<Rigidbody>();
        }

        // Update is called once per frame
        void Update()
        {
            Vector3 forward = transform.forward.normalized;
            // grounded = Physics.Raycast(new Ray(transform.position, -transform.up), 5);
            //
            // if (!grounded)
            //     return;

            if (Input.GetKey(KeyCode.W))
            {
                rb.AddForce(transform.forward * moveSpeed);                
            }
            if (Input.GetKey(KeyCode.A))
            {
                rb.AddTorque(transform.up * -turnSpeed);                
            }
            if (Input.GetKey(KeyCode.D))
            {
                rb.AddTorque(transform.up * turnSpeed);                
            }
        }
    }
}
