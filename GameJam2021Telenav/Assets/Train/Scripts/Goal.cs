using System;
using Train.Scripts;
using UnityEngine;

public class Goal : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<TrainMover>())
        {
            TrainEvents.OnGoalReached?.Invoke();       
        }
    }
}

public static class TrainEvents
{
    public static Action OnGoalReached;
}
