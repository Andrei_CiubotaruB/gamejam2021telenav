using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay {
    public class BotCamera : MonoBehaviour {
        [SerializeField] private float _height;
        [SerializeField] private Transform _target;
        [SerializeField, Range(0, 1)] private float _locationMultiplier;
        [SerializeField] private float _backDistance;
        private Camera _mainCamera;

        public void SetValues(float height, Transform target, float locationMultipler, float backDistance) {
            _target = target;
            _height = height;
            _locationMultiplier = locationMultipler;
            _backDistance = backDistance;
        }

        public void ChangePos(float backDiff, float heightDiff) {
            _backDistance += backDiff;
            _height += heightDiff;
        }

        private void Awake() {
            _mainCamera = Camera.main;
        }

        private void Update() {
            Vector3 pos = transform.position;
            pos.y = _height;

            Vector3 difference = _target.position - pos;
            difference.y = 0;

            _mainCamera.transform.position = pos - difference.normalized * _backDistance;

            pos = _target.position;
            _mainCamera.transform.LookAt(pos + difference * _locationMultiplier);
        }
    }
}
