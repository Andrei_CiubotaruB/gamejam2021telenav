using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay {
    public class CrystalBall : MonoBehaviour {
        [SerializeField] private float _hp;
        [SerializeField] private float _dmgPerVel;
        [SerializeField] private float _dmgAngleMultipler;
        [SerializeField] private Rigidbody _rigidBody;

        private Vector3 _vel;

        public Action EvtCrystalBallBroken;
        public float HP => _hp;

        private void FixedUpdate() {
            if(_rigidBody.velocity.sqrMagnitude > 0)
                _vel = _rigidBody.velocity;
        }

        private void OnCollisionEnter(Collision collision) {
            var speed = _vel.magnitude;
            var angle = Mathf.Abs(Vector3.Dot(collision.contacts[0].normal, _vel) / speed); 
            var angleMultipler = angle * _dmgAngleMultipler;
            var dmg = speed * angleMultipler * _dmgPerVel;

            Debug.LogError("has taken " + dmg.ToString("F3") + " damage");
            _hp -= dmg;

            if (_hp <= 0)
                EvtCrystalBallBroken?.Invoke();
        }
    }
}
