using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RobotBuilder {
    public class Part : MonoBehaviour {
        [SerializeField] private Collider _collider;
        [SerializeField] private Rigidbody _rigidbody;
        [SerializeField] private Material _connectionMaterial;

        [SerializeField] private float _offset;
        private List<Part> _childParts;

        public float Offset
        {
            get => _offset;
            set => _offset = value;
        }

        public Rigidbody Rigidbody => _rigidbody;
        public Material ConnectionMaterial => _connectionMaterial;

        private void Awake() {
            _childParts = new List<Part>();

            foreach (Transform t in transform) {
                Part part;
                if (TryGetComponent(out part)) {
                    _childParts.Add(part);
                }
            }
            if (_rigidbody) {
                _rigidbody.useGravity = false;
            }
        }

        public void EnableGravity() {
            if (!_rigidbody)
                return;
            
            _rigidbody.useGravity = true;
            foreach (var part in _childParts) {
                part.EnableGravity();
            }
        }

        public void Uninitialize() {
            _collider.enabled = false;
        }

        public void Initialize() {
            _collider.enabled = true;
        }
    }
}