using System;

using UnityEngine;
using UnityEngine.UI;

using Persistance;
using UnityEngine.SceneManagement;

namespace Gameplay {
    public class GameplayController : MonoBehaviour {
        [SerializeField] private SelectScreen _newRobotScreenScreen;
        [SerializeField] private Transform _spawnLocation;
        [SerializeField] private LevelEnd _levelEnded;
        [SerializeField] private HighscoreHandler _highscoreHandler;
        [SerializeField] private HighscoreScreen _highscoreScreen;

        [SerializeField] private Text _timeSinceStart;
        [SerializeField] private Text _hpLeft;
        [SerializeField] private CrystalBall _crystalBallPrefab;
        [SerializeField] private float _crystalBallOffset;
        [SerializeField] private float _maxCrystalDistance;

        [SerializeField] private float _height;
        [SerializeField, Range(0, 1)] private float _locationMultiplier;
        [SerializeField] private float _backDistance;
        [SerializeField] private Vector2 _scrollCameraMoveMultipliers;

        private bool _playing;
        private GameObject _currentBot;
        private BotCamera _botCamera;
        private CrystalBall _crystal;
        private float _time = 0;

        private void Start() {
            _newRobotScreenScreen.EvtNewRobotCreated += NewRobotCreated;
            _levelEnded.EvtLevelEnded += RobotReachedEnd;
            _newRobotScreenScreen.ToggleShow();
        }

        private void NewRobotCreated(GameObject bot) {
            if (_currentBot != null)
                Destroy(_currentBot);
            _currentBot = bot;
            var lookDirection = _levelEnded.transform.position;
            lookDirection.y = bot.transform.position.y;
            bot.transform.LookAt(lookDirection);
            bot.transform.position = _spawnLocation.position;

            var botBase = bot.transform.GetChild(0);

            if (_crystal != null)
                Destroy(_crystal);

            _crystal = Instantiate(_crystalBallPrefab);
            RaycastHit hit;
            if(Physics.Raycast(botBase.transform.position+Vector3.up * 5, Vector3.down, out hit, 10)) {
                _crystal.transform.position = hit.point + Vector3.up * _crystalBallOffset;
            } else {
                _crystal.transform.position = botBase.position + Vector3.up * _crystalBallOffset;
            }
            _crystal.EvtCrystalBallBroken += () => {
                _time = float.MaxValue;
                RobotReachedEnd();
                Destroy(_crystal);
            };

            _botCamera = botBase.gameObject.AddComponent<BotCamera>();
            _botCamera.SetValues(_height, _levelEnded.transform, _locationMultiplier, _backDistance);

            foreach (var rb in bot.GetComponentsInChildren<Rigidbody>())
                rb.useGravity = true;

            _time = 0;
            _playing = true;
            _highscoreScreen.gameObject.SetActive(false);
        }

        private void RobotReachedEnd() {
            Destroy(_currentBot);
            _highscoreHandler.PostScores(_currentBot.name, _time);
            _newRobotScreenScreen.ToggleShow();
            _highscoreScreen.gameObject.SetActive(true);
            _playing = false;
        }

        private void Update() {
            if (!_playing)
                return;

            _time += Time.deltaTime;
            _timeSinceStart.text = $"Time: {_time.ToString("F2")}";

            _hpLeft.text = _crystal.HP.ToString("F2");
            if((_currentBot.transform.GetChild(0).transform.position - _crystal.transform.position).sqrMagnitude > _maxCrystalDistance) {
                _time = float.MaxValue;
                RobotReachedEnd();
                Destroy(_crystal);
            }

            if (Input.mouseScrollDelta.y != 0) {
                var diff = Input.mouseScrollDelta.y * Time.deltaTime;
                _botCamera.ChangePos(diff * _scrollCameraMoveMultipliers.x, diff * _scrollCameraMoveMultipliers.y);
            }
        }

        public void ReturnMenu() {
            SceneManager.LoadScene(0);
        }
    }
}