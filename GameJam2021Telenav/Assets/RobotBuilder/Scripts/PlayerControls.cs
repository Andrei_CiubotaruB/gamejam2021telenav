using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RobotBuilder {
    [CreateAssetMenu(fileName = "PlayerControls", menuName = "RobotBuilder/PlayerControls")]
    public class PlayerControls : ScriptableObject {

    }
}