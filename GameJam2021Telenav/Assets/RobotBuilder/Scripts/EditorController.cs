using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Persistance;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace RobotBuilder {
    public class EditorController : MonoBehaviour {
        [SerializeField] private Camera _mainCamera;
        [SerializeField] private float _movementSpeed;
        [SerializeField] private float _zoomSpeed;

        [SerializeField] private InputField _name;
        [SerializeField] private Dropdown _dropdown;
        [SerializeField] private Part[] _parts;
        [SerializeField] private List<Part> _existingParts;

        [SerializeField] private Canvas _canvas;
        [SerializeField] private PistonCreationUI _pistonCreateUI;
        [SerializeField] private SelectScreen _buildsScreen;
        [SerializeField] private SelectScreen _botSaveLoadScreen;

        private Vector3 _botCenter;
        private Dictionary<string, Part> _namesParts;

        private Part _selectedPart;
        private Part _selectedPartPrefab;

        private int _inMenu;

        Vector3 _prevMiddleMouseDown;

        private Part _pistonStart;
        private GameObject _botHolder;

        private void Awake() {
            _dropdown.ClearOptions();

            _namesParts = new Dictionary<string, Part>();
            foreach(var part in _parts) {
                _namesParts.Add(part.name, part);
            }

            _botHolder = new GameObject();

            _botCenter = Vector3.zero;
            foreach(var part in _existingParts) {
                _botCenter += part.transform.position;
                part.transform.SetParent(_botHolder.transform, true);
            }
            _botCenter /= _existingParts.Count;

            _dropdown.AddOptions(_namesParts.Keys.ToList());
            _dropdown.onValueChanged.AddListener(SelectPart);
            SelectPart(0);

            _botSaveLoadScreen.EvtNewRobotCreated += BotLoaded;
        }

        private void BotLoaded(GameObject newBot) {
            Destroy(_botHolder);
            _existingParts.Clear();

            _botHolder = newBot;
            foreach (var part in newBot.GetComponentsInChildren<Part>())
                _existingParts.Add(part);
        }

        private void SelectPart(int index) {
            if(_selectedPart!=null)
                Destroy(_selectedPart.gameObject);

            _selectedPartPrefab = _namesParts[_dropdown.options[_dropdown.value].text];
            _selectedPart = Instantiate(_selectedPartPrefab);
            _selectedPart.Uninitialize();
        }

        private void Update() {
            if (_inMenu > 0 || (_buildsScreen && _buildsScreen.IsOpen))
                return;

            if (Input.GetMouseButtonDown(2)) {
                _prevMiddleMouseDown = Input.mousePosition;
            }

            if (Input.GetMouseButton(2)) {
                var mouseDelta = Input.mousePosition - _prevMiddleMouseDown;

                mouseDelta *= _movementSpeed * Time.deltaTime;
                _mainCamera.transform.Translate(mouseDelta);

                _mainCamera.transform.LookAt(_botCenter, Vector3.up);
                _prevMiddleMouseDown = Input.mousePosition;
            }

            if (Input.mouseScrollDelta.y != 0) {
                _mainCamera.transform.Translate(0, 0, Input.mouseScrollDelta.y * _zoomSpeed * Time.deltaTime);
            }

            if (Input.GetKey(KeyCode.LeftControl)) {
                if (Input.GetKeyDown(KeyCode.UpArrow))
                    _selectedPart.transform.Rotate(90, 0, 0);
                if (Input.GetKeyDown(KeyCode.DownArrow))
                    _selectedPart.transform.Rotate(-90, 0, 0);
                if (Input.GetKeyDown(KeyCode.RightArrow))
                    _selectedPart.transform.Rotate(0, 90, 0);
                if (Input.GetKeyDown(KeyCode.LeftArrow))
                    _selectedPart.transform.Rotate(0, -90, 0);
            }

            if (_selectedPartPrefab != null) {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 100)) {
                    _selectedPart.transform.position = hit.point + hit.normal * _selectedPart.Offset;
                    if (Input.GetKey(KeyCode.LeftControl) && hit.transform.tag == "Part") {
                        if (Input.GetMouseButtonDown(0)) {
                            _pistonStart = hit.transform.GetComponent<Part>();
                        } else if (Input.GetMouseButtonUp(0)) {
                            var pistonTether = hit.transform.GetComponent<Part>();
                            if (pistonTether != _pistonStart) {
                                var pistonCreation = Instantiate(_pistonCreateUI, _canvas.transform);
                                pistonCreation.Initialize(_pistonStart, pistonTether);
                                _inMenu++;
                                pistonCreation.EvtPistonDone += OnPistonMenuDone;
                            } else {
                                foreach(var engine in hit.transform.GetComponents<Engine>()) {
                                    var pistonCreation = Instantiate(_pistonCreateUI, _canvas.transform);
                                    pistonCreation.Initialize(engine);
                                    _inMenu++;
                                    pistonCreation.EvtPistonDone += OnPistonMenuDone;
                                }
                            }
                        }

                        if (Input.GetMouseButtonDown(1)) {
                            foreach (var engine in hit.transform.GetComponents<Engine>())
                                Destroy(engine);
                        }
                    } else {
                        if (Input.GetMouseButtonDown(0)) {
                            _selectedPart.Initialize();
                            _selectedPart.transform.SetParent(_botHolder.transform, true);
                            _existingParts.Add(_selectedPart);

                            _botCenter = Vector3.zero;
                            foreach (var part in _existingParts) {
                                _botCenter += part.transform.position;
                            }
                            _botCenter /= _existingParts.Count;

                            _selectedPart = Instantiate(_selectedPartPrefab);
                            _selectedPart.Uninitialize();
                        } else if (Input.GetMouseButtonDown(1) && hit.transform.tag == "Part" && _existingParts.Count>1) {
                            _existingParts.Remove(hit.transform.GetComponent<Part>());
                            Destroy(hit.transform.gameObject);
                        }
                    }
                } 
            }
        }

        private void OnPistonMenuDone(PistonCreationUI pistonInfo)
        {
            SavedPiston finalizedPiston = pistonInfo.GetPistonData();
            _inMenu--;
        }

        public void SaveBuild() {
            if (string.IsNullOrEmpty(_name.text) || string.IsNullOrWhiteSpace(_name.text))
            {
                Debug.LogWarning("[Aborting] Pleeeease give your poor creation a name before saving");
                return;
            }

            _botHolder.name = _name.text;

            Engine[][] pistons = _existingParts.Select(r => r.GetComponents<Engine>()).Where(x => x!=null).ToArray();
            SavedRobot newSo = new SavedRobot { Name = _name.text, Parts = new SavedPart[_existingParts.Count] };
            int count = pistons.Sum((x) => x.Length);
            for(int i = _existingParts.Count - 1; i >= 0; i--) {
                var part = _existingParts[i];
                newSo.Parts[i] = new SavedPart()
                {
                    Index = i,
                    Type = RemoveCloneNameSection(part.name),

                    Offset = part.Offset, 
                    RBDrag = part.Rigidbody.drag, 
                    RBMass = part.Rigidbody.mass, 
                    RBAngularDrag = part.Rigidbody.angularDrag,
                    
                    Position = part.transform.position,
                    Rotation = part.transform.rotation.eulerAngles
                };
            }

            int pistonIndex = 0; 
            newSo.Pistons = new SavedPiston[count];
            for (int i = pistons.Length - 1; i >= 0; i--)
            {
                Engine[] pistons2 = pistons[i];
                for (int j = 0; j < pistons2.Length; j++)
                {
                    Engine engine = pistons2[j];
                    
                    Assert.IsTrue(TryGetJointParts(engine, out int engineIndex, out int tetherIndex));
                    newSo.Pistons[pistonIndex] = new SavedPiston() 
                    {
                        Index = pistonIndex, //Doesn't matter rn
                        PartEngineIndex = engineIndex,
                        PartTetherIndex = tetherIndex,
                    
                        BackwardsKey = engine.BackwardsKey,
                        ForwardKey = engine.ForwardsKey,
                        Cycle = engine.Cycle,
                        Direction = engine.Direction,
                        Dist = engine.Distance,
                        Force = engine.Force,
                        Power = engine.Power,
                        Spring = engine.Spring
                    };
                    pistonIndex++;
                }
            }

            SaveService.AddBot(newSo);
            SaveService.SaveDataToJson();
        }

        private bool TryGetJointParts(Engine engine, out int engineIndex, out int tetherIndex)
        {
            Part enginePart = engine.GetComponent<Part>();
            Part tether = engine.Joint.connectedBody.gameObject.GetComponent<Part>();

            engineIndex = -1;
            tetherIndex = -1;
            for (int j = _existingParts.Count - 1; j >= 0; j--)
            {
                Part existingPart = _existingParts[j];
                if (existingPart == tether)
                    tetherIndex = j;

                if (existingPart == enginePart)
                    engineIndex = j;
            }
            
            if (engineIndex == -1 || tetherIndex == -1)
            {
                engineIndex = -1;
                tetherIndex = -1;
                return false;
            }
            
            return true;
        }

        private string RemoveCloneNameSection(string partType)
        {
            string result = partType;
            int index = result.IndexOf("(", StringComparison.Ordinal);

            if (index >= 0)
                result = result.Substring(0, index);

            return result;
        }

        public void ReturnMenu() {
            SceneManager.LoadScene(0);
        }

        public void Play() {
            SceneManager.LoadScene(2);
        }
    }
}