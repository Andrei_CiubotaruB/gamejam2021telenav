using System;
using UnityEngine;
using UnityEngine.UI;

namespace Persistance
{
    public class SelectScreen : MonoBehaviour {
        [SerializeField]
        private Canvas canvas;

        [SerializeField]
        private Transform contentParent;

        [SerializeField]
        private RobotButton buttonPrefab;

        [SerializeField]
        private Button saveButton;

        private RobotButton[] buttons;
        private SaveData saveData;

        public bool IsOpen => canvas.enabled;
    
        public Action<GameObject> EvtNewRobotCreated;

        // Start is called before the first frame update
        void Start() {
            saveButton.onClick.AddListener(OnSaveButtonClicked);
            SaveService.OnDataChanged += UpdateButtons;
        }

        private void ShowWindow() {
            UpdateButtons();
        }

        private void UpdateButtons() {
            saveData = SaveService.LoadData();

            if (buttons != null) {
                foreach (RobotButton button in buttons)
                    Destroy(button.gameObject);
            }

            buttons = new RobotButton[saveData.Robots.Count];
            for (int i = 0; i < saveData.Robots.Count; i++) {
                SavedRobot robot = saveData.Robots[i];
                var button = Instantiate(buttonPrefab, contentParent);
                button.Initialize(robot, this);
                button.EvtBotCreated += (x) => {
                    EvtNewRobotCreated?.Invoke(x);
                };
                buttons[i] = button;
            }
        }

        private void Update() {
            // Show/Hide menu
            if (Input.GetKeyDown(KeyCode.Tab))
                ToggleShow();
        }

        public void ToggleShow() {
            canvas.enabled = !canvas.enabled;
            if (canvas.enabled)
                ShowWindow();

            foreach (RobotButton button in buttons)
                button.SetEnabled(canvas.enabled);
        }

        private void OnSaveButtonClicked() {
            UpdateButtons();
        }

        private void OnDestroy() {
            saveButton.onClick.RemoveListener(OnSaveButtonClicked);
            SaveService.OnDataChanged -= UpdateButtons;
        }
    }
}
