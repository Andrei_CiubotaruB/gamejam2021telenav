using System;
using System.Collections.Generic;
using RobotBuilder;
using UnityEngine;
using UnityEngine.UI;

namespace Persistance
{
    public class RobotButton : MonoBehaviour {
        [SerializeField] private Text text;
        [SerializeField] private Button button;
        [SerializeField] private Button deleteButton;
    
        private SelectScreen selectScreen;

        private SavedRobot bot;
        private bool buttonEnabled;

        public Action<GameObject> EvtBotCreated;

        private void Start() {
            button.onClick.AddListener(OnButtonClick);
            deleteButton.onClick.AddListener(OnDeleteButtonClick);
        }

        public void Initialize(SavedRobot robot, SelectScreen parent)
        {
            bot = robot;
            text.text = robot.Name;
            selectScreen = parent;
        }

        private void OnButtonClick() {
            if (!buttonEnabled)
                return;
        
            var robot = AssembleRobot();
            EvtBotCreated?.Invoke(robot);
            selectScreen.ToggleShow();
        }

        private GameObject AssembleRobot() {
            GameObject parent = new GameObject($"Robot [{bot.Name}]");
            parent.transform.SetPositionAndRotation(default, default);

            Vector3 centerPos = Vector3.zero;
            foreach(var part in bot.Parts) {
                centerPos += part.Position;
            }
            centerPos /= bot.Parts.Length;
            parent.transform.position = centerPos;

            Dictionary<int, Part> indexToPart = new Dictionary<int, Part>();

            for (int i = 0; i < bot.Parts.Length; i++) {
                SavedPart part = bot.Parts[i];
                Quaternion savedRotation = Quaternion.identity;
                savedRotation.eulerAngles = part.Rotation;

                GameObject newPart = Instantiate(Resources.Load(part.Type, typeof(GameObject)), part.Position, savedRotation, parent.transform) as GameObject;
                Rigidbody rb = newPart.GetComponent<Rigidbody>();
                Part newPartComponent = newPart.GetComponent<Part>();
                newPartComponent.Offset = part.Offset;
                rb.drag = part.RBDrag;
                rb.mass = part.RBMass;
                rb.angularDrag = part.RBAngularDrag;

                indexToPart.Add(part.Index, newPartComponent);
            }

            for (int i = 0; i < bot.Pistons.Length; i++) {
                SavedPiston piston = bot.Pistons[i];

                var joint = indexToPart[piston.PartEngineIndex].gameObject.AddComponent<ConfigurableJoint>();
                var engine = indexToPart[piston.PartEngineIndex].gameObject.AddComponent<Engine>();
                joint.connectedBody = indexToPart[piston.PartTetherIndex].GetComponent<Rigidbody>();
                engine.SetValues(piston.ForwardKey, piston.BackwardsKey, piston.Power, piston.Direction, piston.Dist, piston.Spring, piston.Force, piston.Cycle, joint);
                engine.Initialize();
            }

            return parent;
        }

        private void OnDeleteButtonClick()
        {
            SaveService.RemoveBot(bot);
            SaveService.SaveDataToJson();
        }

        public void SetEnabled(bool value)
        {
            buttonEnabled = value;
        }

        private void OnDestroy() {
            button.onClick.RemoveListener(OnButtonClick);
            deleteButton.onClick.RemoveListener(OnDeleteButtonClick);
        }
    }
}
