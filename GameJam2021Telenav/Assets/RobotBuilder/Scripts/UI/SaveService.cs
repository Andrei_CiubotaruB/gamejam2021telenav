using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace Persistance
{
    [Serializable]
    public class SaveData {
        public List<SavedRobot> Robots = new List<SavedRobot>();
    }

    [Serializable]
    public struct SavedRobot {
        public string Name;
        public SavedPart[] Parts;
        public SavedPiston[] Pistons;
    }

    [Serializable]
    public struct SavedPart {
        public int Index;
        public string Type;

        public Vector3 Position;
        public Vector3 Rotation;

        public float Offset;
        public float RBMass;
        public float RBDrag;
        public float RBAngularDrag;
    }

    [Serializable]
    public struct SavedPiston {
        public int Index;

        public int PartEngineIndex;
        public int PartTetherIndex;

        public KeyCode ForwardKey;
        public KeyCode BackwardsKey;
        public float Power;
        public Vector3 Direction;
        public float Dist;
        public float Spring;
        public float Force;
        public bool Cycle;
    }

    public class SaveService : MonoBehaviour {
        private static SaveData saveData;

        private const string FILEPATH = "/SaveData/";
        private const string FILENAME = "Robot.txt";

        public static Action OnDataChanged;

        private void Awake()
        {
            saveData = LoadData();
        }

        public static SaveData LoadData() {
            string path = Application.persistentDataPath + FILEPATH + FILENAME;

            if (!File.Exists(path))
                return new SaveData();

            using (StreamReader stream = new StreamReader(path)) {
                string json = stream.ReadToEnd();
                return JsonUtility.FromJson<SaveData>(json);
            }
        }

        public static void SaveDataToJson() {
            string path = Application.persistentDataPath + FILEPATH;
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            saveData ??= new SaveData();


            using (StreamWriter stream = new StreamWriter(path + FILENAME)) {
                string json = JsonUtility.ToJson(saveData);
                stream.Write(json);
            }
        
            OnDataChanged?.Invoke();
        }

        public static void AddBot(SavedRobot robot) {
            saveData ??= new SaveData();
            saveData.Robots ??= new List<SavedRobot>();
            if (!saveData.Robots.Any(x => x.Name == robot.Name))
                saveData.Robots.Add(robot);
        }

        public static void RemoveBot(SavedRobot robot)
        {
            for (var i = 0; i < saveData.Robots.Count; i++)
            {
                SavedRobot savedBot = saveData.Robots[i];
                if (savedBot.Name != robot.Name)
                    continue;
            
                saveData.Robots.Remove(savedBot);
                break;
            }
        }

        [ContextMenu("Write Json")]
        public void TestWrite() {
            SaveService.SaveDataToJson();
        }

        [ContextMenu("Load Json")]
        public void TestLoad() {
            SaveService.LoadData();
        }

    }
}