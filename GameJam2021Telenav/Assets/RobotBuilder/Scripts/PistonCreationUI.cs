using Persistance;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace RobotBuilder {
    public class PistonCreationUI : MonoBehaviour {
        [SerializeField] private InputField _forwardKeyInfo;
        [SerializeField] private InputField _backwardsKeyInfo;
        [SerializeField] private InputField _powerInfo;
        [SerializeField] private InputField _directionInfo;
        [SerializeField] private InputField _distInfo;
        [SerializeField] private InputField _springInfo;
        [SerializeField] private InputField _forceInfo;
        [SerializeField] private Toggle _cycleInfo;
        [SerializeField] private GameObject _cancelButton;

        private KeyCode _forwardKey = KeyCode.A;
        private KeyCode _backwardsKey = KeyCode.S;
        private float _power = 1;
        private Vector3 _direction = new Vector3(0,1,0);
        private float _dist = 3;
        private float _spring = 100;
        private float _force = 3500000000000000;
        private bool _cycle = true;

        public Action<PistonCreationUI> EvtPistonDone;

        Engine _engine;
        ConfigurableJoint _joint;

        public void Initialize(Part pushing, Part tether) {
            _cancelButton.SetActive(true);

            _joint = pushing.gameObject.AddComponent<ConfigurableJoint>();
            _joint.connectedBody = tether.Rigidbody;
            _engine = pushing.gameObject.AddComponent<Engine>();

            _forwardKeyInfo.SetTextWithoutNotify("A");
            _forwardKeyInfo.onEndEdit.AddListener((val) => {
                _forwardKey = (KeyCode) System.Enum.Parse(typeof(KeyCode), val);
            });
            _backwardsKeyInfo.SetTextWithoutNotify("S");
            _backwardsKeyInfo.onEndEdit.AddListener((val) => {
                _backwardsKey = (KeyCode) System.Enum.Parse(typeof(KeyCode), val);
            });
            _powerInfo.SetTextWithoutNotify("1");
            _powerInfo.onEndEdit.AddListener((val) => {
                _power = float.Parse(val);
            });
            _directionInfo.SetTextWithoutNotify("(0,1,0)");
            _directionInfo.onEndEdit.AddListener((val) => {
                _direction = Utils.StringToVector3(val);
            });
            _distInfo.SetTextWithoutNotify("3");
            _distInfo.onEndEdit.AddListener((val) => {
                _dist = float.Parse(val);
            });
            _springInfo.SetTextWithoutNotify("100");
            _springInfo.onEndEdit.AddListener((val) => {
                _spring = float.Parse(val);
            });
            _forceInfo.SetTextWithoutNotify("3500000000000000");
            _forceInfo.onEndEdit.AddListener((val) => {
                _force = float.Parse(val);
            });
            _cycleInfo.isOn = true;
            _cycleInfo.onValueChanged.AddListener((val) => {
                _cycle = val;
            });
        }

        public void Initialize(Engine engine) {
            _cancelButton.SetActive(false);
            _engine = engine;
            _forwardKeyInfo.SetTextWithoutNotify(engine.ForwardsKey.ToString());
            _forwardKeyInfo.onEndEdit.AddListener((val) => {
                _forwardKey = (KeyCode) System.Enum.Parse(typeof(KeyCode), val);
            });
            _backwardsKeyInfo.SetTextWithoutNotify(engine.BackwardsKey.ToString());
            _backwardsKeyInfo.onEndEdit.AddListener((val) => {
                _backwardsKey = (KeyCode) System.Enum.Parse(typeof(KeyCode), val);
            });
            _powerInfo.SetTextWithoutNotify(engine.Power.ToString());
            _powerInfo.onEndEdit.AddListener((val) => {
                _power = float.Parse(val);
            });
            _directionInfo.SetTextWithoutNotify(engine.Direction.ToString());
            _directionInfo.onEndEdit.AddListener((val) => {
                _direction = Utils.StringToVector3(val);
            });
            _distInfo.SetTextWithoutNotify(engine.Distance.ToString());
            _distInfo.onEndEdit.AddListener((val) => {
                _dist = float.Parse(val);
            });
            _springInfo.SetTextWithoutNotify(engine.Spring.ToString());
            _springInfo.onEndEdit.AddListener((val) => {
                _spring = float.Parse(val);
            });
            _forceInfo.SetTextWithoutNotify(engine.Force.ToString());
            _forceInfo.onEndEdit.AddListener((val) => {
                _force = float.Parse(val);
            });
            _cycleInfo.isOn = engine.Cycle;
            _cycleInfo.onValueChanged.AddListener((val) => {
                _cycle = val;
            });
        }

        public void Confirm() {
            _engine.SetValues(_forwardKey, _backwardsKey, _power, _direction, _dist, _spring, _force, _cycle, _joint);
            _engine.Initialize();
            EvtPistonDone.Invoke(this);
            Destroy(gameObject);
        }

        public void Cancel() {
            Destroy(_joint);
            Destroy(_engine);

            EvtPistonDone.Invoke(this);
            Destroy(gameObject);
        }

        public SavedPiston GetPistonData()
        {
            return new SavedPiston()
            {
                BackwardsKey = _backwardsKey, 
                ForwardKey = _forwardKey, 
                Cycle = _cycle, 
                Direction = _direction, 
                Dist = _dist, 
                Force = _force, 
                Power = _power, 
                Spring = _spring
            };
        }
    }
}