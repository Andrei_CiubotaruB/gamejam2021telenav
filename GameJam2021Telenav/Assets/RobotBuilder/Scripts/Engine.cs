using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RobotBuilder {
    public class Engine : MonoBehaviour {
        [SerializeField] private KeyCode _forward;
        [SerializeField] private KeyCode _backward;

        [SerializeField] private float _power;
        [SerializeField] private Vector3 _direction;

        [SerializeField] private float _maxDist;
        [SerializeField] private float _movementSpring;
        [SerializeField] private float _maximumForce;
        [SerializeField] private bool _cycle;

        private LineRenderer _connection;
        private ConfigurableJoint _joint;

        private float _currentDist;
        private int _cycleDirection = 1;

        private bool _initialized;

        public KeyCode ForwardsKey => _forward;
        public KeyCode BackwardsKey => _backward;
        public float Power => _power;
        public Vector3 Direction => _direction;
        public float Distance => _maxDist;
        public float Spring => _movementSpring;
        public float Force => _maximumForce;
        public bool Cycle => _cycle;
        public ConfigurableJoint Joint => _joint;
        
        [SerializeField] private bool _initialize;

        private void OnDestroy() {
            Destroy(_connection);
            Destroy(_joint);
        }

        private void OnValidate() {
            if (_initialize) {
                Initialize();
                _initialize = false;
            }
        }

        public void SetValues(KeyCode forward, KeyCode back, float power, Vector3 direction, float maxDist, float movmSpring, float maxForce, bool cycle, ConfigurableJoint joint) {
            _forward = forward;
            _backward = back;
            _power = power;
            _direction = direction;
            _maxDist = maxDist;
            _movementSpring = movmSpring;
            _maximumForce = maxForce;
            _cycle = cycle;

            if(_joint==null)
                _joint = joint;
        }

        public void Initialize() {
            if (_joint == null) {
                Debug.LogError("Should have had joint here");
                _joint = GetComponent<ConfigurableJoint>();
            }

            _joint.angularXMotion = ConfigurableJointMotion.Locked;
            _joint.angularYMotion = ConfigurableJointMotion.Locked;
            _joint.angularZMotion = ConfigurableJointMotion.Locked;
            _joint.xMotion = ConfigurableJointMotion.Limited;
            _joint.yMotion = ConfigurableJointMotion.Limited;
            _joint.zMotion = ConfigurableJointMotion.Limited;

            _joint.xDrive = new JointDrive() { positionSpring = _movementSpring, maximumForce = _maximumForce };
            _joint.yDrive = new JointDrive() { positionSpring = _movementSpring, maximumForce = _maximumForce };
            _joint.zDrive = new JointDrive() { positionSpring = _movementSpring, maximumForce = _maximumForce };
            _joint.linearLimit = new SoftJointLimit() { limit = _maxDist, bounciness = 0, contactDistance = 0 };

            if (!_initialized) {
                _connection = new GameObject("LineRenderer").AddComponent<LineRenderer>();
                _connection.material = GetComponent<Part>().ConnectionMaterial;
                _connection.positionCount = 2;
            }

            _initialized = true;
        }

        private void Update() {
            if (!_initialized)
                return;

            float power = _power * Time.deltaTime;

            if (Input.GetKey(_forward)) {
                if (_cycle) {
                    _currentDist += power * _cycleDirection;
                    if(_currentDist>_maxDist || _currentDist < 0) {
                        _currentDist = Mathf.Clamp(_currentDist + power, 0, _maxDist);
                        _cycleDirection *= -1;
                    }
                } else {
                    _currentDist = Mathf.Clamp(_currentDist + power, 0, _maxDist);
                }
            } else if (Input.GetKey(_backward)) {
                if (_cycle) {
                    _currentDist -= power * _cycleDirection;
                    if (_currentDist > _maxDist || _currentDist < 0) {
                        _currentDist = Mathf.Clamp(_currentDist + power, 0, _maxDist);
                        _cycleDirection *= -1;
                    }
                } else {
                    _currentDist = Mathf.Clamp(_currentDist - power, 0, _maxDist);
                }
            }

            _joint.targetPosition = _direction * _currentDist;

            _connection.SetPositions(new Vector3[] { transform.position, _joint.connectedBody.transform.position });
        }
    }
}