using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighscoreInfo : MonoBehaviour
{
    [SerializeField] private Text nameText;
    [SerializeField] private Text scoreText;

    public void Initialize(string playerName, float score)
    {
        nameText.text = playerName;
        scoreText.text = score.ToString("F2");
    }
}
