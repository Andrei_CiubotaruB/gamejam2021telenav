using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighscoreScreen : MonoBehaviour
{
    [SerializeField] 
    private Canvas canvas;
    
    [SerializeField] 
    private Transform contentParent;

    [SerializeField] 
    private HighscoreInfo highscorePrefab;

    [SerializeField]
    private HighscoreHandler highscoreHandler;

    private HighscoreInfo[] highscores;
    
    public bool IsOpen => canvas.enabled;

    void OnEnable()
    {
        UpdateScores();
    }

    private void Update()
    {
        // Show/Hide menu
        //if (Input.GetKeyDown(KeyCode.Tilde)) 
        //    ToggleShow();

        //if (Input.GetKeyDown(KeyCode.P)) 
        //    highscoreHandler.PostScores("Juanito", Random.Range(0, 100));
    }
    
    public void ToggleShow()
    {
        canvas.enabled = !canvas.enabled;
        if (canvas.enabled) 
            UpdateScores();
    }
    
    private void UpdateScores()
    {
        if (highscores != null && highscores.Length > 0)
        {
            foreach (HighscoreInfo scoreObject in highscores) 
                Destroy(scoreObject.gameObject);
        }
        
        List<Score> scores = highscoreHandler.RetrieveScores();
        highscores = new HighscoreInfo[scores.Count];

        for (int i = 0; i < scores.Count; i++) {
            Score score = scores[i];
            highscores[i] = Instantiate(highscorePrefab, contentParent);
            highscores[i].Initialize(score.name, score.score);
        }
    }
}
