using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Highscore", menuName = "RobotBuilder/HighScores")]
public class HighScoreData : ScriptableObject {
    [SerializeField] public List<Score> ExistingHighScores;
}
