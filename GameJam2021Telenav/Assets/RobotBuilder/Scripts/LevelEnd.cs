using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay {
    public class LevelEnd : MonoBehaviour {
        public Action EvtLevelEnded;
        private void OnCollisionEnter(Collision collision) {
            if (collision.transform.tag == "Part") {
                EvtLevelEnded?.Invoke();
            }
        }
    }
}